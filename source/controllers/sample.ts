import { Request, Response, NextFunction } from 'express';
import logging from '../config/logging';

const NAMESPACE = 'Sample Controller';

const sampleHeathCheck = (req: Request, res: Response, next: NextFunction) => {
    logging.info(NAMESPACE, `Sample Healt Check Call Api`);
    return res.status(200).json({
        message: 'ping'
    });
};

export default { sampleHeathCheck };
